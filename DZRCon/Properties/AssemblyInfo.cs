﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Общие сведения об этой сборке предоставляются следующим набором
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("DZRCon")]
[assembly: AssemblyDescription("DZRCon DayZ Server Restarter")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("DayZ Russia")]
[assembly: AssemblyProduct("DZRCon DayZ Server Restarter")]
[assembly: AssemblyCopyright("Copyright © DayZRussia.com 2021")]
[assembly: AssemblyTrademark("DayZ Russia")]
[assembly: AssemblyCulture("")]

// Установка значения False для параметра ComVisible делает типы в этой сборке невидимыми
// для компонентов COM. Если необходимо обратиться к типу в этой сборке через
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("fd069c2d-86e7-4bd9-929c-e68bdc8aaff1")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номер сборки и номер редакции по умолчанию.
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyFileVersion("1.0.41.2311")]
[assembly: NeutralResourcesLanguage("en")]

